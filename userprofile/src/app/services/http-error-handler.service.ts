import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
// import 'rxjs/add/observable/throw';
// import { LoggerService } from './log4ts/logger.service';
import { throwError } from 'rxjs';
// import { _throw } from 'rxjs/observable/throw';


import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HttpErrorHandlerService {

  constructor(private router: Router) { }

  handleError(serviceName = '', error: HttpErrorResponse, endpoint = 'operation', isAlert = true) {

    if (error.status === 401 || error.status === 403) {
      this.router.navigate(['/login']);
    }
    return throwError(error);

  }
}
