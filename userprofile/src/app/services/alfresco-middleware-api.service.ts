import { Injectable } from '@angular/core';
import { AlfrescoApiService, AuthenticationService } from '@alfresco/adf-core';
import { Router } from '@angular/router';
import { HttpErrorHandlerService } from './http-error-handler.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlfrescoMiddlewareApiService {

  constructor(private alfAuthuthService: AuthenticationService,
    private router: Router,
    private apiService: AlfrescoApiService,
    public httpErrorHandler: HttpErrorHandlerService,
    public http: HttpClient
  ) { }


  postWebscript(URL, data) {

    if (!this.alfAuthuthService.isEcmLoggedIn()) {
      this.router.navigate(['/login']);
      return;
    }
    return new Promise((resolve, reject) => {
      this.apiService.getInstance().webScript.executeWebScript('POST', URL, null, null, null, data)
        .then(function (res) {
          return resolve(res);
        }).catch(error => {
          return reject(this.httpErrorHandler.handleError('api-service', error, URL));
        });
    });
  }

  getWebScript(URL) {

    if (!this.alfAuthuthService.isEcmLoggedIn()) {
      this.router.navigate(['/login']);
      return;
    }

    return new Promise((resolve, reject) => {
      this.apiService.getInstance().webScript.executeWebScript('GET', URL)
        .then(function (res) {
          return resolve(res);
        }).catch(error => {
          return reject(this.httpErrorHandler.handleError('api-service', error, URL));
        });
    });
  }


  deleteWebScript(URL) {

    if (!this.alfAuthuthService.isEcmLoggedIn()) {
      this.router.navigate(['/login']);
      return;
    }

    return new Promise((resolve, reject) => {
      this.apiService.getInstance().webScript.executeWebScript('DELETE', URL)
        .then(function (res) {
          return resolve(res);
        }).catch(error => {
          return reject(this.httpErrorHandler.handleError('api-service', error, URL));
        });
    });
  }


  putWebscript(URL, data) {

    if (!this.alfAuthuthService.isEcmLoggedIn()) {
      this.router.navigate(['/login']);
      return;
    }
    return new Promise((resolve, reject) => {
      this.apiService.getInstance().webScript.executeWebScript('PUT', URL, null, null, null, data)
        .then(function (res) {
          return resolve(res);
        }).catch(error => {
          return reject(this.httpErrorHandler.handleError('api-service', error, URL));
        });
    });
  }

}
