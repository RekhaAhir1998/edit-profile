import { Injectable } from '@angular/core';
import { AlfrescoMiddlewareApiService } from './alfresco-middleware-api.service';

@Injectable({
  providedIn: 'root'
})
export class EdituserService {

  constructor(private alfrescoMiddleware: AlfrescoMiddlewareApiService) { }
  edituserdata(postData, uname) {
    return new Promise((resolve, reject) => {
      this.alfrescoMiddleware.putWebscript('api/people/' + uname, postData)
        .then(function (data) {
          // debugger;
          return resolve(data);
        }).catch(err => {
          return reject(err);
        });
    });
  }
  getimage() {
    return new Promise((resolve, reject) => {
      this.alfrescoMiddleware.getWebScript('slingshot/profile/avatar/rekha')
        .then(function (data) {
           debugger;
          return resolve(data);
        }).catch(err => {
          return reject(err);
        });
    });
  }
  // updateimg(postData,name)
  // {
  //   return new Promise((resolve, reject) => {
  //     this.alfrescoMiddleware.postWebscript('slingshot/profile/uploadavatar', postData,name)
  //       .then(function (data) {
  //         // debugger;
  //         return resolve(data);
  //       }).catch(err => {
  //         return reject(err);
  //       });
  //   }); 
  // }
}
