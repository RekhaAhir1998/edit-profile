import { Component, OnInit } from '@angular/core';
import { PeopleContentService, IdentityUserService, AlfrescoApiService } from '@alfresco/adf-core';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserData } from 'app/models/user';
import { EdituserService } from 'app/services/edituser.service';
import { PeopleApi } from '@alfresco/js-api';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']

})
export class EditProfileComponent implements OnInit {
  UpdateForm: FormGroup;
  userInfo;
  firstName = '';
  temp = false;
  showhide = false;
  userdata: UserData;
  fileData: File = null;
  reader;

  constructor(
    private fb: FormBuilder,
    private userservice: EdituserService,
    private people: PeopleContentService,
    private identityservice: IdentityUserService,
    private alf: AlfrescoApiService,
    private http: HttpClient,
    private snackbar: MatSnackBar
  ) {
    this.people.getCurrentPerson().subscribe(res => {
      this.userInfo = res.entry;
      this.UpdateForm.patchValue({
        firstName: this.userInfo.firstName,
        lastName: this.userInfo.lastName,
        jobTitle: this.userInfo.jobTitle,
        location: this.userInfo.location,
        description: this.userInfo.description,
        telephone: this.userInfo.telephone,
        mobile: this.userInfo.mobile,
        email: this.userInfo.email,
        skypeId: this.userInfo.skypeId,
        instantMessageId: this.userInfo.instantMessageId,
        googleId: this.userInfo.googleId,
        organization: this.userInfo.company.organization,
        postcode: this.userInfo.company.postcode,
        address1: this.userInfo.company.address1,
        address2: this.userInfo.company.address2,
        address3: this.userInfo.company.address3,
        telephone2: this.userInfo.company.telephone,
        fax: this.userInfo.company.fax,
        email2: this.userInfo.company.email
      });
    });
  }


  ngOnInit() {
    this.UpdateForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: [''],
      jobTitle: [''],
      location: [''],
      description: [''],
      telephone: ['',Validators.maxLength(10)],
      mobile: ['',  Validators.maxLength(10)],
      email: ['', [Validators.required, Validators.email]],
      skypeId: [''],
      instantMessageId: [''],
      googleId: [''],
      organization: [''],
      postcode: [''],
      address1: [''],
      address2: [''],
      address3: [''],
      telephone2: ['', Validators.maxLength(10)],
      fax: [''],
      email2: ['', [ Validators.email]]
    });
  }

  get o() { return this.UpdateForm.controls; }

  onSubmit() {
    this.temp = true;
    if (this.UpdateForm.invalid) {
      this.snackbar.open('Please Enter Valid Data.', '', { duration: 9000 });
      return;
    } else {
      this.snackbar.open('Data Updated Sucessfully!', '', { duration: 9000 });
    }
    const companyData = {
      'organization': this.UpdateForm.get('organization').value,
      'address1': this.UpdateForm.get('address1').value,
      'address2': this.UpdateForm.get('address2').value,
      'address3': this.UpdateForm.get('address3').value,
      'postcode': this.UpdateForm.get('postcode').value,
      'telephone': this.UpdateForm.get('telephone2').value,
      'fax': this.UpdateForm.get('fax').value,
      'email': this.UpdateForm.get('email2').value
    }
    this.userdata = {
      'firstName': this.UpdateForm.get('firstName').value,
      'lastName': this.UpdateForm.get('lastName').value,
      'description': this.UpdateForm.get('description').value,
      'email': this.UpdateForm.get('email').value,
      'skypeId': this.UpdateForm.get('skypeId').value,
      'googleId': this.UpdateForm.get('googleId').value,
      'instantMessageId': this.UpdateForm.get('instantMessageId').value,
      'jobTitle': this.UpdateForm.get('jobTitle').value,
      'location': this.UpdateForm.get('location').value,
      'company': companyData,
      'mobile': this.UpdateForm.get('mobile').value,
      'telephone': this.UpdateForm.get('telephone').value
    };
    const result = Object.assign({}, this.UpdateForm.value);
    this.alf.peopleApi.updatePerson(this.userInfo.id, this.userdata, '').then(res => {
    
    });
    const HttpUploadOptions = {
      headers: new HttpHeaders({ 'Accept': 'multipart/form-data' })
      }
      let input = new FormData();
      const postdata =
      {
      filedata: this.fileData,
      username: 'rekha'
      
      }
      const url = 'http://localhost:8080/alfresco/s/slingshot/profile/uploadavatar';
      input.append('filedata', this.fileData);
      input.append('username', 'rekha');
      
      this.http.post(url, postdata, HttpUploadOptions).subscribe((res => {
      debugger;
      }));
    this.UpdateForm.reset();
  }
  onSelectedChange(event) {
    console.log("hello")
    if (event.target.files.length > 0) {
    this.reader = new FileReader();
    this.reader.readAsDataURL(event.target.files[0]);
    this.reader.onload = (event) => {
    this.fileData = event.target.result;
    console.log(this.fileData);
    }
    };
    }
}
