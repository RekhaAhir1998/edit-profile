import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditProfileComponent } from './edit-profile.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
@NgModule({
  declarations: [EditProfileComponent],
  exports:
  [EditProfileComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    
  ]
})
export class EditProfileModule { }
