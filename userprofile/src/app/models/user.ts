export class UserData {
    firstName: string;
    lastName: string;
    jobTitle: string;
    location: string;
    telephone: string;
    mobile: string;
    email: string;
    skypeId: string;
    instantMessageId: string;
    googleId: string;
    description: string;

    company: {
        organization: string;
        address1: string;
        address2: string;
        address3: string;
        postcode: string;
        telephone: string;
        fax: string;
        email: string;
    };
}
